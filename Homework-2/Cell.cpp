#include "Cell.h"
#include <iostream>

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene) {
	this->_glocus_receptor_gene = glucose_receptor_gene;

	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
}

bool Cell::get_ATP() {
	std::string rna_transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);

	Protein* new_protein = this->_ribosome.create_protein(rna_transcript);

	if(!(new_protein)) {
		std::cerr << "[!] error creating new Protein";
		_exit(1);
	}

	this->_mitochondrion.insert_glucose_receptor(*new_protein);
	this->_mitochondrion.set_glucose(50);

	if (!(this->_mitochondrion.produceATP())) {
		return false;
	}
	this->_atp_utints = 100;
	return true;
}